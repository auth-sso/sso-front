# sso-front

## ディレクトリ構成

### pages

- Next プロジェクト生成時に自動生成されるディレクトリ。
- ページとなるコンポーネントを配置する。
- ファイル名に沿って自動でルーティングが設定される。

### components

- アプリケーション全体で使用される共通コンポーネントを配置する。
- 頻繁に再利用されることを想定し、基本的にドメイン情報（アプリケーション固有の情報）を含ませることはせずに、再利用性を十分に考慮したコンポーネントを配置する。
- ディレクトリは UI のタイプ別で作成する。（参考: [MUI - Components](https://mui.com/material-ui/getting-started/installation/))
- `例: inputs/Button.tsx, feedback/Toast.tsx`

### features

- アプリケーション固有の機能となるファイルを配置する。
- 一つの feature ディレクトリは components, hooks といったタイプ別ディレクトリでさらに分類し、その機能に関するファイルが集約されるようにする。
- `例: auth/components/LoginForm.tsx, auth/types.ts`

### 参考

- [Project Structure](https://github.com/alan2207/bulletproof-react/blob/master/docs/project-structure.md)
